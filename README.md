Hebrew Year Progress
====================
Twitter bot inspired by [@year_progress](https://twitter.com/year_progress).  
This bot publish the progress of the Hebrew calendar year.
  
<div class="center">
<blockquote class="twitter-tweet" data-lang="en"><p lang="und" dir="ltr">▓▓▓▓▓▓▓▓▓░░░░░░░░░░░ 43%</p>&mdash; Hebrew Year Progress (@yearProgressHeb) <a href="https://twitter.com/yearProgressHeb/status/1099773897254359040?ref_src=twsrc%5Etfw">February 24, 2019</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</div>

## TODO  

- [ ] Add tests
- [ ] Add full CI/CD
- [ ] Add auto tweets on Jewish holidays
